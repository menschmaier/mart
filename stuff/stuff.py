import os
import time
from stuff.config_loader import CheckerSettings

def pather(checkername):
    path = CheckerSettings.result_path
    replace_dic = {"$current": str(os.getcwd()), "$checker": checkername}
    for x in replace_dic:
        path = path.replace(x, replace_dic[x])
    path = str(time.strftime(path))
    if not os.path.exists(path):
        os.makedirs(path)
    return path


class stuff:
    BASIC_PATH = str(os.getcwd())
    CURRENT_TIME = str(time.strftime('[%d-%m-%Y %H-%M-%S]'))
    COMBO_PATH = BASIC_PATH + "/combos.txt"
    COMBOS = [x for x in open(BASIC_PATH + "/combos.txt", encoding="utf8", errors='ignore').readlines() if ":" in x]
    COUNTER = 0
class Proxies:
    proxies_json = {}
    proxies_working_counter = 0
    proxies_checked = 0

