from multiprocessing.dummy import Pool as ThreadPool
import threading
import time
from stuff.config_loader import ProxycheckerSettings as PS
from stuff.stuff import stuff, Proxies
import requests
import json
import sys
import socks
proxy_file = [x for x in open(stuff.BASIC_PATH + "/proxies.txt", encoding="utf8", errors='ignore').readlines() if ":" in x]
proxy_types = ["http", "socks4", "socks5"]





def proxy_checker():
    running = True
    def status_thread():
        z = 0
        while running:
            z = z + 1
            time.sleep(0.3)
            print("Proxies checking status: " + str(Proxies.proxies_checked) + "/" + str(len(proxy_file)) + " Working: " + str(Proxies.proxies_working_counter), end="\r")

    t1 = threading.Thread(target=status_thread, args=[])
    t1.daemon = True
    t1.start()
    try:
        myip = str(requests.get(PS.own_ip_getter_url).content.decode())
    except:
        myip = "127.0.0.1"
    def check(x):
        y = 0
        proxy = proxy_file[x].replace("\n", "")
        while True:
            if y > 2:
                break
            try:
                if y == 0:
                    proxy_dict = {
                        'http': "http://" + proxy,
                        'https': "https://" + proxy
                    }
                elif y > 0:
                    proxy_dict = {
                        'http': proxy_types[y] + "://" + proxy,
                        'https': proxy_types[y] + "://" + proxy
                    }
                y = y + 1
                r = requests.get(PS.proxy_judge_url, proxies=proxy_dict, timeout=PS.timeout_in_ms / 1000)
                rem_add = r.content.decode().split("REMOTE_ADDR = ")[1].split("REQUEST_SCHEME")[0].replace("\n", "")
                if r.content.decode().__contains__(myip):
                    transparent = True
                else:
                    transparent = False
                number = len(Proxies.proxies_json)
                Proxies.proxies_json[number] = {}
                Proxies.proxies_json[number]["address"] = proxy
                Proxies.proxies_json[number]["transparent"] = transparent
                Proxies.proxies_json[number]["remote_address"] = rem_add
                Proxies.proxies_json[number]["type"] = proxy_types[y - 1]
                Proxies.proxies_json[number]["cloudflare_captcha"] = None
                Proxies.proxies_working_counter = Proxies.proxies_working_counter + 1
                break
            except Exception as e:
                pass
        Proxies.proxies_checked = Proxies.proxies_checked + 1

    def theads_two(numbers, threads=5):
        pool = ThreadPool(threads)
        results = pool.map(check, numbers)
        pool.close()
        pool.join()
        return results
    if __name__ == "checkers.proxychecker":
        countt = []
        for x in range(len(proxy_file)):
            countt.append(int(x))

        threads_one = theads_two(countt, PS.thread_amount)
    json_data = json.dumps(Proxies.proxies_json, sort_keys=False, indent=4)
    running = False
    time.sleep(0.5)
    print("\nFinished Proxy checking")
